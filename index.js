'use strict'
//cargamos la libreria que ayudará a la conexión
var mongoose = require('mongoose');
var app = require('./app');
var port = 3900;

mongoose.Promise = global.Promise;
//esta libreria recibirá dos parametros la url y opciones, la url es la que nos arroja mongo db, posterior se creará la ruta del api
mongoose.connect('mongodb://0.0.0.0:27017/api_rest_blog', { useUnifiedTopology: true}).
    then(()=> {
        console.log('La conexión a la base de datos se ha realizado correctamente');

        //crear servidor y escuchar peticiones HTTP
        app.listen(port, () => {
            console.log('Servidor corriendo en http://localhost:'+port);
        })
});